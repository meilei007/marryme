function WeddingTime() {
	var now, kickoff, diff;
	now      = new Date();
	kickoff  = new Date("October 6, 2013 12:00:00");
	diff = kickoff - now;

	days  = Math.floor( diff / (1000*60*60*24) );
	hours = Math.floor( diff / (1000*60*60) );
	mins  = Math.floor( diff / (1000*60) );
	secs  = Math.floor( diff / 1000 );

	dd = days;
	hh = hours - days  * 24;
	mm = mins  - hours * 60;
	ss = secs  - mins  * 60;

	document.getElementById("ledtimer")
		.innerHTML = dd + '<span>Days</span> ' + hh + '<span>Hours</span> ' + mm + '<span>Minutes</span> ' + ss + '<span>Seconds</span>';
}
setInterval('WeddingTime()', 1000 );