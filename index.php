<!DOCTYPE html>
<html lang="zh-CN">
    <head>
		<meta charset="UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
        <title>王燕嫁给梅磊吧</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"> 
        <meta name="description" content="王燕嫁给梅磊吧" />
        <meta name="keywords" content="" />
        <meta name="author" content="Xiaopeng" />
        <link rel="shortcut icon" href="http://www.llavie.com/img/favicon.ico">
        <link rel="stylesheet" type="text/css" href="love/css/demo.css" />
        <link rel="stylesheet" type="text/css" href="love/css/style.css" />
		<script type="text/javascript" src="love/js/jquery.min.js"></script>
		<script type="text/javascript" src="love/js/modernizr.custom.79639.js"></script> 
		<script type="text/javascript" src="love/js/modernizr.custom.26887.js"></script> 
		<script src="love/js/plugins.js" type="text/javascript"></script>
		<script type="text/javascript">
			$(function() {
				$("#contactform").validate({
					rules: {
						name: {
							required: true
						},
						email: {
							required: true,
							email: true
						},
						message: {
							required: true
						}
					},
					highlight: function(element, errorClass, validClass) {
						$(element).addClass(errorClass).removeClass(validClass);
						$(element.form).find("label[for=" + element.id + "]")
							.addClass(errorClass);
					},
					unhighlight: function(element, errorClass, validClass) {
						$(element).removeClass(errorClass).addClass(validClass);
						$(element.form).find("label[for=" + element.id + "]")
							.removeClass(errorClass);
					},
				});	
			});
		</script>
        <script type="text/javascript" src="love/js/jquery.transit.min.js"></script>
		<script type="text/javascript" src="love/js/jquery.gridrotator.js"></script>
		<script type="text/javascript">	
			$(function() {
				$( '#ri-grid' ).gridrotator( {
					rows		: 6,
					columns		: 8,
					animType	: 'fadeInOut',
					animSpeed	: 1000,
					interval	: 600,
					step		: 1,
					w320		: {
						rows	: 3,
						columns	: 4
					},
					w240		: {
						rows	: 3,
						columns	: 4
					}
				} );
			
			});
		</script>
		<!--[if lte IE 8]>
			 <link rel="stylesheet" type="text/css" href="love/css/simple.css" />
		<![endif]-->
		<!-- 微博验证 -->
		<!-- <meta property="wb:webmaster" content="3fe78578ec2a2fb4" /> -->
		<meta property="wb:webmaster" content="3fe78578ec2a2fb4" />

		<!-- 百度统计 -->


    </head>
    <body>
        <!--[if lt IE 9]><div style="position:fixed;top:0;left:0;right:0;bottom:0;background:black;z-index:999999999;text-align:center;"><a href="http://www.llavie.com/godarkforie-cn/"><img src="http://www.llavie.com/godarkforie-cn/img/zh-cn.jpg" alt="您正在使用旧版本的Internet Explorer浏览器" /></a></div><![endif]-->
		
		<div class="container">
			<div id="author"><p>Designed by <a href="http://www.hualinfor.com" target="_blank">Hualinfor</a></p></div>
			<div id="ledtimer"></div>
			<script type="text/javascript" src="love/js/weddingtimer.js"></script>
			<div class="st-container">

				<input type="radio" name="radio-set" checked="checked" id="st-control-1"/>
				<a href="#st-panel-1">我们的空间</a>
				<input type="radio" name="radio-set" id="st-control-2"/>
				<a href="#st-panel-2">我们的故事</a>
				<input type="radio" name="radio-set" id="st-control-3"/>
				<a href="#st-panel-3">时光留影</a>
				<input type="radio" name="radio-set" id="st-control-4"/>
				<a href="#st-panel-4">给你的婚礼</a>
				<input type="radio" name="radio-set" id="st-control-5"/>
				<a href="#st-panel-5">朋友们的祝福</a>
				<div class="st-scroll">
					
					<section class="st-panel st-color" id="st-panel-1">
						<h2>王燕,嫁给我吧！</h2>
						<p>2006.3.26~2013.10.6 - 近八年的恋爱马拉松，我对你的爱，亘久不变，嫁给我！亲爱的！</p>
					</section>
					
					<section class="st-panel st-color" id="st-panel-2">
						<h2>火车上的相遇 ~ 校园里的相恋</h2>
						<p>蒹葭苍苍，白露为霜。所谓伊人，在水一方。/ 溯洄从之，道阻且长。溯游从之，宛在水中央。/ 蒹葭萋萋，白露未曦。所谓伊人，在水之湄。/ 溯洄从之，道阻且跻。溯游从之，宛在水中坻。/ 蒹葭采采，白露未已。所谓伊人，在水之涘。/ 溯洄从之，道阻且右。溯游从之，宛在水中沚。【蒹葭（诗经·秦风）】</p>
					</section>
					
					<section class="st-panel" id="st-panel-3">
						<div id="ri-grid" class="ri-grid ri-grid-size-2">
							<ul>
								<?php 
									$i=1;
									while($i<=50)
									{
										echo "<li><a href=" . '""' . "><img src=" . '"images/medium/' . $i . '.jpg"'. "/></a></li>";
										$i++;
									}
								?>	
							</ul>
						</div>
						
					</section>
					
					<section class="st-panel st-color" id="st-panel-4">
						<h2>为你准备的婚礼</h2>
						<p>新郎-梅磊 / 新娘-王燕<br /> &nbsp &nbsp &nbsp 时间：2013年10月6日（九月初二)</p>
					</section>
					
					<section class="st-panel" id="st-panel-5">
						
<!-- 							<form id="contactform" method="post" action="send.php">
								<fieldset>
									<div id="theFields">
										<input id="name" name="name" type="text" class="txtfield" placeholder="您的姓名" />
										<label for="name"><span></span></label>

										<input id="email" name="email" type="email" class="txtfield" placeholder="您的邮箱" />
										<label for="email"><span></span></label>
										
										<textarea id="message" name="message" placeholder="我们想收到您的祝福哦！"></textarea>
										<label for="message" ><span style="margin-top: 30px;"></span></label>

										<input type="submit" value="寄出您的祝福 ^_^" class="btn" />
									</div>
								</fieldset>
							</form> -->

					<!-- weibo 评论 -->
					<h1>欢迎围观，留下您的祝福吧!</h1>

					<script type="text/javascript">
						// (function(){
						// var url = "http://widget.weibo.com/distribution/comments.php?width=290&url=http%3A%2F%2Fmeilei.net.cn&border=1&brandline=y&skin=10&appkey=2964076317&iframskin=10&dpc=1";
						// document.write('<iframe id="WBCommentFrame" src="' + url + '" scrolling="yes" frameborder="0" style="width:308px; height:548px"></iframe>');
						// })();
						// </script>
						// <script type="text/javascript">
						// window.WBComment.init({
						//     "id": "WBCommentFrame"
						// });
					</script>


					<script>
						// placeholder polyfill
						$(document).ready(function(){
							function add() {
								if($(this).val() == ''){
									$(this).val($(this).attr('placeholder')).addClass('placeholder');
								}
							}
							function remove() {
								if($(this).val() == $(this).attr('placeholder')){
									$(this).val('').removeClass('placeholder');
								}
							}
							// Create a dummy element for feature detection
							if (!('placeholder' in $('<input>')[0])) {
								// Select the elements that have a placeholder attribute
								$('input[placeholder], textarea[placeholder]').blur(add).focus(remove).each(add);
							}
						});
					</script>
					</section>

				</div><!-- // st-scroll -->
				
			</div><!-- // st-container -->
			
        </div>

		<script language="JavaScript" type="text/javascript">
			<!--
			// PLAYER VARIABLES

			var mp3snd = "./love/cantstoplove.mp3";
			var bkcolor = "000000";

			if ( navigator.userAgent.toLowerCase().indexOf( "msie" ) != -1 ) {
			document.write('<bgsound src="'+mp3snd+'" loop="-1">');
			}
			else if ( navigator.userAgent.toLowerCase().indexOf( "firefox" ) != -1 ) {
			document.write('<object data="'+mp3snd+'" type="application/x-mplayer2" width="0" height="0">');
			document.write('<param name="filename" value="'+mp3snd+'">');
			document.write('<param name="autostart" value="1">');
			document.write('<param name="playcount" value="infinite">');
			document.write('</object>');
			}
			else {
			document.write('<audio src="'+mp3snd+'" autoplay="autoplay" loop="loop">');
			document.write('<object data="'+mp3snd+'" type="application/x-mplayer2" width="0" height="0">');
			document.write('<param name="filename" value="'+mp3snd+'">');
			document.write('<param name="autostart" value="1">');
			document.write('<embed height="2" width="2" src="'+mp3snd+'" pluginspage="http://www.apple.com/quicktime/download/" type="video/quicktime" controller="false" controls="false" autoplay="true" autostart="true" loop="true" bgcolor="#'+bkcolor+'"><br>');
			document.write('</embed></object>');
			document.write('</audio>');
			}
			//-->
		</script>
		



	</body>
</html>
